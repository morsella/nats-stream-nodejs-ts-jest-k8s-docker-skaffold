import express, { Request, Response } from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import { NotFoundError, errorHandler } from '@moresella/common';

import { currentUserRouter } from './routes/current-user';
import { logoutRouter } from './routes/logout';
import { loginRouter } from './routes/login';
import { signupRouter } from './routes/signup';


const app  = express();
app.set('trust proxy', true); //traffic proxied via ingress 
app.use(json());
app.use(
    cookieSession({
        signed: false,
        secure: process.env.NODE_ENV !== 'test'
    })
);
app.use(currentUserRouter);
app.use(loginRouter);
app.use(logoutRouter);
app.use(signupRouter);

app.all('*', async(req: Request, res: Response) => {
    throw new NotFoundError();
});

app.use(errorHandler);

export { app };