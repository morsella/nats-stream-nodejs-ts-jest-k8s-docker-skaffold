import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { requestValidation, BadRequestError } from '@moresella/common';
import jwt from 'jsonwebtoken';

import { Organization } from '../models/organization';
import { User } from '../models/user';

const router = express.Router();

router.post('/api/users/signup', 
[
    body('email').isEmail().withMessage('Please provide a valid email'),
    body('password').trim().isLength({min: 8}).withMessage('Please provide 8 characters password'),
    body('organizationName').trim().isLength({min: 2}).withMessage('Please provide company name')
], requestValidation, 
async (req: Request, res: Response) => {
        const { email, password, organizationName } = req.body; 

        const existingUser = await User.findOne({ email });
        if (existingUser) {
            throw new BadRequestError('Email in use');
        }
        const existingOrganinzation = await Organization.findOne({ organizationName });
        const companies = await Organization.find({});

        if (!existingOrganinzation && companies.length >= 1 ) {
            throw new BadRequestError('No company found');
        }
        let organization;
        if(companies.length < 1){
            organization = await Organization.build({
                organizationName: 'MorSella',
                city: 'Lelystad',
                zipcode: '1234AA',
                street: 'Some Street 1',
                phone: '06123456',
                country: 'NL'
            }).save();
        }

        const organizationId = organization? organization.id : existingOrganinzation.id;
        const user = await User.build({ email, password, organizationId }).save();
        //Create JWT 
        const userJwt = jwt.sign(
            {
            userUUID: user.userUUID,
            email: user.email
            }, process.env.JWT_KEY!
        );
        
        // Store it on session object
        req.session = {
            jwt: userJwt,
        };
        res.status(201).send({user});
});

export { router as signupRouter };