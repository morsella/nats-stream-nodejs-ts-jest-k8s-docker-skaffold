import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { requestValidation, BadRequestError } from '@moresella/common';
import jwt from 'jsonwebtoken';
import { Password } from '../util/password';
import { User } from '../models/user';

const router = express.Router();

router.post('/api/users/login', [
    body('email').isEmail().withMessage('Please provide a valid email'),
    body('password').trim().notEmpty().withMessage('Please provide a password')
], requestValidation,
async (req: Request, res: Response) => {
    const { email, password } = req.body;

    const existingUser = await User.findOne({ email });
    if (!existingUser) {
      throw new BadRequestError('Invalid Email');
    }

    const passwordsMatch = await Password.compare(
      existingUser.password,
      password
    );
    if (!passwordsMatch) {
      throw new BadRequestError('Invalid Password');
    }

    // Create JWT
    const userJwt = jwt.sign(
      {
        userUUID: existingUser.userUUID,
        email: existingUser.email,
      },
      process.env.JWT_KEY!
    );

    // Save in Session
    req.session = {
      jwt: userJwt,
    };

    res.status(200).send(existingUser);
});

export { router as loginRouter };