import request from 'supertest';
import { app } from '../../app';

it('201 on signup success', async () => {
  return request(app)
    .post('/api/users/signup')
    .send({
      email: 'test@test.com',
      password: 'password',
      organizationName: 'company'
    })
    .expect(201);
});

it('400 error with invalid email', async () => {
    return request(app)
    .post('/api/users/signup')
    .send({
      email: 'testtest.com',
      password: 'password',
      organizationName: 'company'
    })
    .expect(400);
});

it('400 error with invalid password', async () => {
    return request(app)
    .post('/api/users/signup')
    .send({
      email: 'test@test.com',
      password: 'pass',
      organizationName: 'company'
    })
    .expect(400);
});

it('400 with duplicate emails', async () => {
    await request(app)
      .post('/api/users/signup')
      .send({
        email: 'test@test.com',
        password: 'password',
        organizationName: 'company'
      })
      .expect(201);
  
    await request(app)
      .post('/api/users/signup')
      .send({
        email: 'test@test.com',
        password: 'password',
        organizationName: 'company'
      })
      .expect(400);
  });

  it('201 with cookie after successful signup', async () => {
    const response = await request(app)
      .post('/api/users/signup')
      .send({
        email: 'test@test.com',
        password: 'password',
        organizationName: 'company'
      })
      .expect(201);
    expect(response.get('Set-Cookie')).toBeDefined();
  });