import request from 'supertest';
import { app } from '../../app';

it('400 whith an email that does not exist', async () => {
    await request(app)
      .post('/api/users/login')
      .send({
        email: 'test@test.com',
        password: 'password'
      })
      .expect(400);
  });

  it('400 with the incorrect password', async () => {
    await request(app)
        .post('/api/users/signup')
        .send({
            email: 'test@test.com',
            password: '1234Abcd',
            organizationName: 'company'
        })
        .expect(201)
    await request(app)
        .post('/api/users/login')
        .send({
            email: 'test@test.com',
            password: '4455223'
        })
        .expect(400);
  });

  it('201 with a cookie when given valid credentials', async () => {
    await request(app)
      .post('/api/users/signup')
      .send({
        email: 'test@test.com',
        password: 'password',
        organizationName: 'company'
      })
      .expect(201);
  
    const response = await request(app)
      .post('/api/users/login')
      .send({
        email: 'test@test.com',
        password: 'password'
      })
      .expect(200);
  
    expect(response.get('Set-Cookie')).toBeDefined();
  });