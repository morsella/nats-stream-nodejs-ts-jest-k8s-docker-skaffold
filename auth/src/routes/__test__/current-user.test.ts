import request from 'supertest';
import { app } from '../../app';

it('200 with details about the current user', async () => {
    const cookie = await global.login();
    const response = await request(app)
        .get('/api/users/currentuser')
        .set('Cookie', cookie)
        .send()
        .expect(200);

  expect(response.body.currentUser.email).toEqual('test@test.com');
});

it('200 with', async () => {
    const response = await request(app)
      .get('/api/users/currentuser')
      .send()
      .expect(200);
  
    expect(response.body.currentUser).toEqual(null);
  });