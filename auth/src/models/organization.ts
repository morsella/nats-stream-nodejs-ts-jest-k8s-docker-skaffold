import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

//An interface that describes the properties
//that are required to create a new user

interface OrganizationAttrs {
    organizationName: string;
    city: string;
    zipcode: string;
    street: string;
    phone: string;
    country: string;
}

//An interface that describes the properties that User Model has
interface OrganizationModel extends mongoose.Model<any> {
    build(attrs: OrganizationAttrs): OrganizationDoc;
}

//An interface that describes the properties that User document has
interface OrganizationDoc extends mongoose.Document {
    organizationName: string;
    city: string;
    zipcode: string;
    street: string;
    phone: string;
    country: string;
    createdAt: string;
}

const organizationSchema = new mongoose.Schema({
    organizationName: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    zipcode: {
        type: String,
        required: true
    },
    street: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    }
},{
    toJSON: {
        transform(doc, ret) {
          ret.id = ret._id;
          delete ret._id;
          delete ret.__v;
        }
      }
});

organizationSchema.statics.build = (attrs: OrganizationAttrs) => {
    return new Organization(attrs);
}

const Organization = mongoose.model<OrganizationDoc, OrganizationModel>('Organization', organizationSchema);

export { Organization };