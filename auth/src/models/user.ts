import mongoose, { Schema } from 'mongoose';
import { Password } from '../util/password';
import { v4 as uuidv4 } from 'uuid';

//An interface that describes the properties
//that are required to create a new user

interface UserAttrs {
    email: string;
    password: string;
    organizationId: string;
}


//An interface that describes the properties that User document has
interface UserDoc extends mongoose.Document {
    userUUID: string;
    email: string;
    password: string;
    organizationId: string;
}

//An interface that describes the properties that User Model has
interface UserModel extends mongoose.Model<any> {
    build(attrs: UserAttrs): UserDoc;
}

const userSchema = new mongoose.Schema({
    // _id: { 
    //     type: String, 
    //     unique: true,
    //     default: uuidv4()
    // },
    email: {
        type: String,
        unique: true,
        required : true, 
    },
    password: {
        type: String,
        required: true
    },
    organizationId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Organization'
    },
    userUUID: {
        type: Schema.Types.String,
        required : true,
        unique: true,
        default: uuidv4()
    }
},
{
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
        delete ret.__v;
        delete ret.organizationId;
      }
    },
    // _id: false,
    timestamps: true
});

userSchema.pre('save', async function(done) {
    if (this.isModified('password')) {
      const hashed = await Password.toHash(this.get('password'));
      this.set('password', hashed);
    }
    done();
});

userSchema.statics.build = (attrs: UserAttrs) => {
    const user = new User(attrs);
    return user;
}

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

export { User };