/**
 * @jest-environment jsdom
 */
import { render, screen } from "@testing-library/react";
import LandingPage from "../pages/index";
import Application from "../pages/challenge/icecaps";
import request from 'supertest';

it('renders the current value', async () => {
  render(<LandingPage />);
  const title = screen.getByRole("heading", { name: "Hey!" })
  expect(title).toBeInTheDocument();
});

it('renders the challenge icecaps', async () => {
  render(<Application />);
  const title = screen.getByRole("button", { name: "Subscribe User" })
  expect(title).toBeInTheDocument();
});
