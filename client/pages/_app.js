import 'bootstrap/dist/css/bootstrap.css';
import buildBase from '../api/build-base';
import HeaderComponent from '../components/header';

const AppComponent  = ({ Component, pageProps, currentUser }) => {
    return (
        <div>
            <HeaderComponent currentUser={currentUser} />
            <div className="container">
            <Component currentUser={currentUser} {...pageProps} />
            </div>
        </div>
    );
};

AppComponent.getInitialProps = async (appContext) => {
    const base = buildBase(appContext.ctx);
    const { data }  = await base.get('/api/users/currentuser');

    let pageProps= {};
    if(appContext.Component.getInitialProps) {
        pageProps = await appContext.Component.getInitialProps(appContext.ctx);
    }
    return {
        pageProps,
        ...data
    }
}

export default AppComponent;

