import { useEffect } from 'react';
import Router from 'next/router';
import useRequest from '../../hooks/use-request';
import LoginComponent from './login';

const LogoutComponent = () => {
  const { doRequest } = useRequest({
    url: '/api/users/logout',
    method: 'post',
    body: {},
    onSuccess: () => Router.push('/')
  });

  useEffect(() => {
    doRequest();
  }, []);

  return <div>Log out...</div>;
};

export default LogoutComponent;