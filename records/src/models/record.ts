import mongoose, { Schema } from 'mongoose';

interface RecordAttrs {
    title: string;
    price: number;
    recordDate: string;
    userUUID: string;
}

interface RecordDoc extends mongoose.Document{
    title: string;
    price: number;
    recordDate: string;
    userUUID: string;
    createdAt: string;
    updatedAt: string;
}

interface RecordModel extends mongoose.Model<any>{
    build(attrs: RecordAttrs): RecordDoc;
}

const recordSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    recordDate: {
        type: Date,
        required: true
    },
    userUUID: {
        type: Schema.Types.String,
        required: true,
        ref: 'User'
    }
},
{
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      }
    }
});


recordSchema.statics.build = (attrs: RecordAttrs) => {
    return new Record(attrs);
}

const Record = mongoose.model<RecordDoc, RecordModel>('Record', recordSchema);

export { Record };