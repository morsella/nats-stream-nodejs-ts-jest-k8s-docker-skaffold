import express, { Request, Response } from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';
import { NotFoundError, errorHandler, currentUser } from '@moresella/common';

import { createRecordRouter } from './routes/create-record'; 
import { getRecordsRouter } from './routes/get-records';
import { getRecordRouter } from './routes/get-record'; 
import { updateRecordRouter } from './routes/update-record';

const app  = express();
app.set('trust proxy', true); //traffic proxied via ingress 
app.use(json());
app.use(
    cookieSession({
        signed: false,
        secure: process.env.NODE_ENV !== 'test'
    })
);
app.use(currentUser);

app.use(createRecordRouter);
app.use(getRecordsRouter);
app.use(getRecordRouter);
app.use(updateRecordRouter);

app.all('*', async(req: Request, res: Response) => {
    throw new NotFoundError();
});

app.use(errorHandler);

export { app };