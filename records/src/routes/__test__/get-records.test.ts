import request from 'supertest';
import { app } from '../../app';

it('200 showing all records for the same user', async () => {
    //global login for user
    const userCookie = global.login();
   //create few records
    await global.newRecord(userCookie);
    await global.newRecord(userCookie, 'Record 2', 20);
    await global.newRecord(userCookie, 'Record 3', 5);
    
    const response = await request(app).get('/api/records').send().expect(200);

    expect(response.body.length).toEqual(3);
});