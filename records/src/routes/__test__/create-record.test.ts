import request from 'supertest';
import { app } from '../../app';
import { Record } from '../../models/record';
import { natsClient } from '../../nats-client';

it('!404 route listens on /api/records', async () => {
    const response = await request(app)
        .post('/api/records')
        .send({});
    expect(response.status).not.toEqual(404);
});
it('401 without current user', async () => {
    const response = await request(app)
        .post('/api/records')
        .send({});
    expect(response.status).toEqual(401);
});
it('200 only access with current user', async () => {
    const today = new Date();
    const tomorrow = today.setDate(today.getDate() + 1);

  const result =   await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            recordDate: tomorrow,
            price: 10
        })
        .expect(200);
});
it('400 error with invalid date and time', async () => {
    const today = new Date();
    const yesterday = today.setDate(today.getDate() - 1);
    await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            recordDate: '',
            price: 10,
        })
        .expect(400)
    await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            recordDate: yesterday,
            price: 10,
        })
        .expect(400)
    await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            price: 10,
        })
        .expect(400)
});
it('400 error with invalid price', async () => {
    const today = new Date();
    const tomorrow = today.setDate(today.getDate() + 1);

    await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            recordDate: tomorrow,
            price: -10,
        })
        .expect(400)
    await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            recordDate: tomorrow
        })
        .expect(400)
});
it('200 create a record', async () => {
    const today = new Date();
    const tomorrow = today.setDate(today.getDate() + 1);
    let records = await Record.find({});
    expect(records.length).toEqual(0);

    await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            recordDate: tomorrow,
            price: 10
        })
        .expect(200);
    
    records = await Record.find({});
    expect(records.length).toEqual(1);
});
it('publishes an event', async () => {
    const today = new Date();
    const tomorrow = today.setDate(today.getDate() + 1);

    await request(app)
        .post('/api/records')
        .set('Cookie', global.login())
        .send({
            title: 'Some event',
            recordDate: tomorrow,
            price: 30
        })
        .expect(200);

    expect(natsClient.client.publish).toHaveBeenCalled();
});