import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

it('200 showing record with id', async () => {
    //global login for user
    const userCookie = global.login();
    //create new record
    const title = 'New Record';
    const price = 22.50;
    const record = await global.newRecord(userCookie, title, price);

    const response = await request(app)
                            .get(`/api/records/${record.body.id}`)
                            .send().expect(200);

    expect(response.body.title).toEqual(title);
    expect(response.body.price).toEqual(price);
});

it('404 record not found', async () => {
    //generate random mongoose ObjectId
    const id = new mongoose.Types.ObjectId().toHexString();
    await request(app).get(`/api/records/${id}`).send().expect(404);
});