import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';
import { natsClient } from '../../nats-client';

it('404 if no record found', async () => {
    //generate a random mongoos ObjectId
    const id = new mongoose.Types.ObjectId().toHexString();

    //details to update
    const today = new Date();
    const newRecordDate = today.setDate(today.getDate() + 2);
    
    await request(app).put(`/api/records/${id}`)
                        .set('Cookie', global.login())
                        .send({ 
                            recordDate: newRecordDate,
                            title: 'Some title',
                            price: 12
                        }).expect(404);
});

it('401 if no current user', async () => {
    //generate a random mongoos ObjectId
    const id = new mongoose.Types.ObjectId().toHexString();

    //details to update
    const today = new Date();
    const newRecordDate = today.setDate(today.getDate() + 2);
    
    await request(app).put(`/api/records/${id}`)
                        .send({ 
                            recordDate: newRecordDate,
                            title: 'Some title',
                            price: 12
                        }).expect(401);
});

it('401 if the user is Unauthorized user for this record', async () => {
    //create a user
    const user = global.login();

    //create a record
    const record = await global.newRecord(user);

    //details to update
    const today = new Date();
    const newRecordDate = today.setDate(today.getDate() + 2);
    
    await request(app).put(`/api/records/${record.body.id}`)
                        .set('Cookie', global.login())
                        .send({ 
                            recordDate: newRecordDate,
                            title: 'Some title',
                            price: 12
                        }).expect(401);
});

it('400 with wrong title, price and date', async () => {
    //create a user
    const user = global.login();

    //create a record
    const record = await global.newRecord(user);

    //details to update
    const today = new Date();
    const newRecordDate = today.setDate(today.getDate() + 2);
    
    await request(app).put(`/api/records/${record.body.id}`)
                        .set('Cookie', user)
                        .send({ 
                            recordDate: today.setDate(today.getDate() - 2),
                            title: 'Some title',
                            price: 12
                        }).expect(400);
    await request(app).put(`/api/records/${record.body.id}`)
                        .set('Cookie', user)
                        .send({ 
                            recordDate: newRecordDate,
                            title: '',
                            price: 12
                        }).expect(400);
    await request(app).put(`/api/records/${record.body.id}`)
                        .set('Cookie', user)
                        .send({ 
                            recordDate: newRecordDate,
                            title: 'Some title',
                            price: 0
                        }).expect(400);
});

it('200 record is updated', async () => {
    //create a user
    const user = global.login();

    //create a record
    const record = await global.newRecord(user);

    //details to update
    const today = new Date();
    const newRecordDate = today.setDate(today.getDate() + 2);
    
    await request(app).put(`/api/records/${record.body.id}`)
                        .set('Cookie', user)
                        .send({ 
                            recordDate: newRecordDate,
                            title: 'Some title',
                            price: 12
                        }).expect(200);
});
it('publishes an event', async () => {
    //create a user
    const user = global.login();

    //create a record
    const record = await global.newRecord(user);

    //details to update
    const today = new Date();
    const newRecordDate = today.setDate(today.getDate() + 2);
    
    await request(app).put(`/api/records/${record.body.id}`)
                        .set('Cookie', user)
                        .send({ 
                            recordDate: newRecordDate,
                            title: 'Some title1',
                            price: 25
                        }).expect(200);

    expect(natsClient.client.publish).toHaveBeenCalled();
});