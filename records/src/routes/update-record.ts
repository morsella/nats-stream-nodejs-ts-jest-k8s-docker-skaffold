import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import {
  requestValidation,
  NotFoundError,
  checkAuth,
  UnauthorizedError,
  BadRequestError,
} from '@moresella/common';
import { Record } from '../models/record';
import { RecordUpdatedPublisher } from '../events/publishers/record-updated-publisher';
import { natsClient } from '../nats-client';


const router = express.Router();

router.put(
  '/api/records/:id',
  checkAuth,
  [
    body('recordDate').not().isEmpty().custom((value, {req})=>{
        const now = new Date();  
        const providedDate = new Date(value);  
        if (now > providedDate) {    
            throw new Error("Date or Time are not valid");    
        }
        return true;
    }),
    body('title').not().isEmpty().withMessage('Title is required'),
    body('price')
      .isFloat({ gt: 0 })
      .withMessage('Price must be greater than 0'),
  ],
  requestValidation,
  async (req: Request, res: Response) => {
    const record = await Record.findById(req.params.id);

    if (!record) {
      throw new NotFoundError();
    }

    if (record.userUUID !== req.currentUser!.userUUID) {
      throw new UnauthorizedError();
    }

    record.set({
      title: req.body.title,
      price: req.body.price,
    });
    await record.save();
    console.log('record', record);

    new RecordUpdatedPublisher(natsClient.client).publish({
      id: record.id,
      title: record.title,
      price: record.price,
      userUUID: record.userUUID,
      recordDate: record.recordDate
    });
    res.send(record);
  }
);

export { router as updateRecordRouter };