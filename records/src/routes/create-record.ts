import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { checkAuth, requestValidation } from '@moresella/common';
import { Record } from '../models/record';
import { RecordCreatedPublisher } from '../events/publishers/record-created-publisher';
import { natsClient } from '../nats-client';

const router = express.Router();

router.post('/api/records', checkAuth, [
    body('recordDate').not().isEmpty().custom((value, {req})=>{
        const now = new Date();  
        const providedDate = new Date(value);  
        if (now > providedDate) {    
            throw new Error("Date or Time are not valid");    
         }
        return true;
    }),
    body('price').isFloat({ gt: 0 }).withMessage('Price must be greater then 0'),
    body('title').not().isEmpty().withMessage('Please give title to the record')
], requestValidation, 
async (req: Request, res: Response) => {
    const { title, price, recordDate } = req.body; 

    console.log(req.body);
    console.log(req.currentUser);
    const userUUID = req.currentUser!.userUUID;
    const record = await Record.build({
        title,
        price, 
        recordDate,
        userUUID
    }).save();

    console.log('record', record);
    new RecordCreatedPublisher(natsClient.client).publish({
        id: record.id,
        title: record.title,
        price: record.price,
        userUUID: record.userUUID,
        recordDate: record.recordDate
    });
    res.status(200).send(record);
});

export { router as createRecordRouter }