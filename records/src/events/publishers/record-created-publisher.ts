import { Publisher, Subjects, RecordCreatedEvent } from '@moresella/common';

export class RecordCreatedPublisher extends Publisher<RecordCreatedEvent> {
    readonly subject = Subjects.RecordCreated;
}