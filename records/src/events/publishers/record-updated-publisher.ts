import { Subjects, Publisher, RecordUpdatesEvent } from '@moresella/common';

export class RecordUpdatedPublisher extends Publisher<RecordUpdatesEvent> {
    readonly subject = Subjects.RecordUpdated;
}