import mongoose from 'mongoose';
import { app } from './app';
import { natsClient } from './nats-client';
import nats, { Stan } from 'node-nats-streaming';


const start = async () => {
    if (!process.env.JWT_KEY) {
        throw new Error('JWT_KEY must be defined');
    }
    if (!process.env.MONGO_URI) {
        throw new Error('MONGO URI is undefined');
    }
    if (!process.env.NATS_CLIENT_ID) {
        throw new Error('NATS_CLIENT_ID is undefined');
    }
    if (!process.env.NATS_CLUSTER_ID) {
        throw new Error('NATS_CLUSTER_ID is undefined');
    }
    if (!process.env.NATS_URL) {
        throw new Error('NATS_URL is undefined');
    }
    try {
        console.log(process.env.NATS_CLUSTER_ID,
            process.env.NATS_CLIENT_ID,
            process.env.NATS_URL);
       await natsClient.connect(
            process.env.NATS_CLUSTER_ID,
            process.env.NATS_CLIENT_ID,
            process.env.NATS_URL
        );
        natsClient.client.on('close', () => {
            console.log('Close NATS');
            process.exit();
        });
        process.on('SIGINT', () => natsClient.client.close());
        process.on('SIGTERM', () => natsClient.client.close());
        
        await mongoose.connect(process.env.MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        console.log("connected to MONGODB");
    } catch (err) {
        console.log('ERROR', err);
    }
    app.listen(3000, () => {
        console.log('Listening on port 3000');
    });
}

start();