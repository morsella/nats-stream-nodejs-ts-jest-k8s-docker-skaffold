import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import request, { Response } from 'supertest';
import { app } from '../app';
import jwt from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';

declare global {
    namespace NodeJS {
      interface Global {
        login(): string[];
        newRecord(userCookie: string[], title?: string, price?: number): Promise<Response>;
      }
    }
}

jest.mock('../nats-client');

let mongo: any;
beforeAll(async () => {
    process.env.JWT_KEY = 'testkeyjwt';
    // process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

    mongo = new MongoMemoryServer();
    const mongoUri = await mongo.getUri();

    await mongoose.connect(mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
});

beforeEach(async () => {
    jest.clearAllMocks();
    const collections = await mongoose.connection.db.collections();

    for(let collection of collections){
        await collection.deleteMany({});
    }
});

afterAll(async () => {
    await mongo.stop();
    await mongoose.connection.close();
});

global.login = () => {
  //Build a JWT payload
  const payload = {
    userUUID: uuidv4(),
    email: 'test@test.com' 
  }
  //create the JWT 
  const token = jwt.sign(payload, process.env.JWT_KEY!);

  //Build session object {jwt: MY_JWT}
  const session  = {jwt: token}

  //Turn that session into JSON
  const sessionJSON = JSON.stringify(session);

  //Take JSON and encode it as base64
  const base64 = Buffer.from(sessionJSON).toString('base64');

  //return string that has cookie decoded data that appears the same as in the browser
  return [`express:sess=${base64}`];

};

global.newRecord = async (userCookie: string[], title = 'Record 1', price = 10) => {
  // record date 
  const today = new Date();
  const tomorrow = today.setDate(today.getDate() + 1); 

  return await request(app).post('/api/records')
                      .set('Cookie', userCookie)
                      .send({
                          title: title,
                          recordDate: tomorrow,
                          price: price
                      })
                      .expect(200);
};